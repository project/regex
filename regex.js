
var RegEx = RegEx || {};

RegEx.ToggleReplacementVisibility = function() {
  // Extract the chosen function name
  var $selector = "form#regex-regex-tester-form #edit-function";
  var $fun = $($selector).attr("value");

  // Change the visibility input field based on the presence of '_replace'
  // string in its name.
  //$selector = "form#regex-regex-tester-form :input#edit-replacement";
  $selector = "form#regex-regex-tester-form #edit-replacement-wrapper";
  if ($fun.match("_replace") != null ) {
    $($selector).show("fast");
  }
  else 	{
    $($selector).hide("fast");
  }
}

RegEx.ToggleFlags = function() {
  // Extract the name of the chosen function.
  var $selector = "form#regex-regex-tester-form #edit-function";
  var $fun = $($selector).attr("value");

  // All flags
  var $flags = new Array(
    "PREG_PATTERN_ORDER",
    "PREG_SET_ORDER",
    "PREG_OFFSET_CAPTURE",
    "PREG_SPLIT_NO_EMPTY",
    "PREG_SPLIT_DELIM_CAPTURE",
    "PREG_SPLIT_OFFSET_CAPTURE",
    "mb_ereg_replace_i",
    "mb_ereg_replace_x",
    "mb_ereg_replace_m",
    "mb_ereg_replace_p",
    "mb_ereg_replace_e",
    "javascript_global",
    "javascript_ignorecase",
    "javascript_multiline"
  );
  // Options depending on the choice of function
  $flags_to_show = new Array();

  if ($fun.match("preg_match")) {
    $flags_to_show[0] = "PREG_PATTERN_ORDER";
    $flags_to_show[1] = "PREG_SET_ORDER";
    $flags_to_show[2] = "PREG_OFFSET_CAPTURE";
  }
  else if ($fun.match("preg_split")) {
    $flags_to_show[0] = "PREG_SPLIT_DELIM_CAPTURE";
    $flags_to_show[1] = "PREG_SPLIT_NO_EMPTY";
    $flags_to_show[2] = "PREG_SPLIT_OFFSET_CAPTURE";
  }
  else if ($fun.match("mb_ereg_replace|mb_eregi_replace")) {
    $flags_to_show[0] = "mb_ereg_replace_i";
    $flags_to_show[1] = "mb_ereg_replace_x";
    $flags_to_show[2] = "mb_ereg_replace_m";
    $flags_to_show[3] = "mb_ereg_replace_p";
    $flags_to_show[4] = "mb_ereg_replace_e";
  }
  else if ($fun.match("javascript")) {
    $flags_to_show[0] = "javascript_global";
    $flags_to_show[1] = "javascript_ignorecase";
    $flags_to_show[2] = "javascript_multiline";
  }


  for ($flag in $flags) {
    $f = $flags[$flag];
    $flag_id = "edit-flags-" + $f.replace(/_/g, "-") + "-wrapper";

    $selector = "form#regex-regex-tester-form div[@id="+$flag_id+"]";
    if ($flags_to_show.indexOf($f) > -1) {
      $($selector).show("fast");
    }
    else 	{
      $($selector).hide("fast");
    }
  }
  $selector = "form#regex-regex-tester-form div[@id=edit-flags-PREG-PATTERN-ORDER-wrapper]";
  if( $flags_to_show.length ) {
    // Show all the hidden divs containing flags
    $($selector).parent().parent().show("fast");
  }
  else {
    $($selector).parent().parent().hide("fast");
  }
}


RegEx.ExecuteJavaScript = function() {
  var $fun         = $("form#regex-regex-tester-form #edit-function").attr("value");

  if ($fun.match("javascript") == null) {
    return false;
  }

  var $pattern     = $("form#regex-regex-tester-form #edit-pattern").attr("value");
  var $replacement = $("form#regex-regex-tester-form #edit-replacement").attr("value");
  var $subject     = $("form#regex-regex-tester-form #edit-subject").attr("value");
  var $modifiers = "";

  if ($("form#regex-regex-tester-form #edit-flags-javascript-global").is(":checked")) {
    $modifiers = $modifiers + "g";
  }

  if ($("form#regex-regex-tester-form #edit-flags-javascript-ignorecase").is(':checked')) {
    $modifiers = $modifiers + "i";
  }

  if ($("form#regex-regex-tester-form #edit-flags-javascript-multiline").is(':checked')) {
    $modifiers = $modifiers + "m";
  }

  if ($pattern.match(/^[\s\t\r\n]*$/) || $subject.match(/^[\s\t\r\n]*$/)) {
    $("form#regex-regex-tester-form #regex-messages").html("");
    return false;
  }

  var $output = "";

  switch ($fun) {
    case "javascript_exec" :
      var $regexp = new RegExp($pattern, $modifiers);
      var $matches = $regexp.exec($subject);
      while ($matches != null) {
        $output = $output + "<div class=\"messages status\">";
        $.each($matches, function(key, value) {
          $output = $output + key+" = "+value+"<br />";
        });
        $matches = $regexp.exec(RegExp.rightContext);
        $output = $output + "</div>";
      }
      if ($output.length == 0) {
        $output = $output + "<div class=\"messages warning\">" + Drupal.t("No match") + "</div>";
      }
      break;

    case 'javascript_search' :
      break;

    case "javascript_replace":
      break;

    case "javascript_split":
      break;

    default :
      $output = "";
      break;

  }

  $("form#regex-regex-tester-form #regex-messages").html($output);
}

$(document).ready(
  function() {
    // Initialize the position
    RegEx.ToggleReplacementVisibility();
    RegEx.ToggleFlags();

    // Update things when the function selector is changed.
    $("form#regex-regex-tester-form #edit-function").change( function() {
        RegEx.ToggleReplacementVisibility();
        RegEx.ToggleFlags();
      }
    );

    $("#edit-pattern, #edit-replacement, #edit-subject, #edit-function, #edit-flags-javascript-multiline, #edit-flags-javascript-global, #edit-flags-javascript-ignorecase", "form#regex-regex-tester-form").change( function() {
        RegEx.ExecuteJavaScript();
      }
    );
  }
);


